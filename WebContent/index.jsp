<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:bundle basename="resources.Resources">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><fmt:message key="string.page.title"/></title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<jsp:include page="templates/header.jsp"/>
<c:if test="${not empty sessionScope.error}">
	<p class="error"><c:out value="${sessionScope.error}"/></p>
	<c:remove var="error" scope="session" />
</c:if>
<c:if test="${not empty sessionScope.message}">
	<p class="message"><c:out value="${sessionScope.message}"/></p>
	<c:remove var="message" scope="session" />
</c:if>

<div class="content">
	<c:choose>
	<c:when test="${not empty sessionScope.user}">
		<h2><fmt:message key="string.hi"/> <c:out value="${sessionScope.user.username}"/>!</h2>
		<p><fmt:message key="string.logged"/></p>
		<ul>
			<li><fmt:message key="string.user.id"/>: <c:out value="${sessionScope.user.userId}" /></li>
			<li><fmt:message key="string.user.firstname"/>: <c:out value="${sessionScope.user.firstName}" /></li>
			<li><fmt:message key="string.user.secondname"/>: <c:out value="${sessionScope.user.secondName}" /></li>
			<li><fmt:message key="string.user.email"/>: <c:out value="${sessionScope.user.email}"/></li>
		</ul>
		<form action="Login">
			<button type="submit" name="action" value="logout"><fmt:message key="string.logout"/></button>
		</form>
	</c:when>
	<c:otherwise>
		<h2><fmt:message key="string.login"/></h2>
		<form action="Login" method="post">
		<label>
			<fmt:message key="string.username"/>:
			<input	type="text"
					name="username"
					required
					value="<c:out value='${sessionScope.username}' default=""/>"
					placeholder="<fmt:message key="string.username"/>"/>
		</label>
		<br/>
		<label>
			<fmt:message key="string.password"/>:
			<input	type="password"
					name="password"
					required
					placeholder="<fmt:message key='string.password'/>"/>
		</label>
		<br/>
		<button type="submit" name="action" value="login"><fmt:message key='string.login'/></button>
		</form>
	</c:otherwise>
</c:choose>
</div>

</fmt:bundle>
<jsp:include page="templates/footer.jsp"/>