<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:include page="../templates/header.jsp"/>
<fmt:bundle basename="resources.Resources">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><fmt:message key="string.title.list"/></title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/list.css">
</head>
<body>
<div class="content">
	<h2><fmt:message key="string.title.list"/></h2>
	<table>
		<thead>
			<th><fmt:message key="string.user.id"/></th><th><fmt:message key="string.user.username"/></th><th><fmt:message key="string.user.email"/></th><th><fmt:message key="string.action.edit"/></th><th><fmt:message key="string.action.delete"/></th>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.userList}" var="user" >
				<tr>
					<td><a href="user?action=view&userId=${user.userId}">${user.userId}</a></td>
					<td>${user.username}</td>
					<td>${user.email}</td>
					<td><a href="user?action=edit&userId=${user.userId}">Edit</a></td>
					<td><a href="user?action=delete&userId=${user.userId}">Delete</a></td>
				</tr>
		 	</c:forEach>
		</tbody>
	</table>
</div>
</fmt:bundle>
<jsp:include page="../templates/footer.jsp"/>