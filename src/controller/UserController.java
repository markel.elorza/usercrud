package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.user.dao.UserFacade;
import domain.user.model.User;

/**
 * Servlet implementation class UserController
 */
@WebServlet("/user")
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String action = request.getParameter("action");
	    String requestedPage = "";
	    switch (action) {
			case "list":
				requestedPage = listUsers(request);
				break;
			case "create":
				requestedPage = createUsersForm(request);
				break;
			case "edit":
				requestedPage = editUser(request, request.getParameter("userId"));
				break;
			case "view":
				requestedPage = viewUser(request, request.getParameter("userId"));
				break;
			case "delete":
				requestedPage = deleteUser(request, request.getParameter("userId"));
				break;
			default:
				break;
		}
	    if (requestedPage.equals("delete")) {
	    	response.sendRedirect("user?action=list");
	    }else {
	    	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(requestedPage);
	        dispatcher.forward(request, response);
	    }
	    
	}


	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String action = request.getParameter("action");
	    String requestedPage = "";
	    switch (action) {
			case "create":
				requestedPage = createUser(request);
				break;
			case "edit":
				requestedPage = editUser(request);
				break;
			default:
				break;
		}
	    
	    response.sendRedirect(requestedPage);
	}
	
	private String deleteUser(HttpServletRequest request, String userId) {
		UserFacade uf = new UserFacade();
		uf.deleteUser(Integer.parseInt(userId));
		return "delete";
	}
	
	private String listUsers(HttpServletRequest request) {
		ArrayList<User> users = null;
		UserFacade uf = new UserFacade();
		users = uf.loadUsers();
		request.setAttribute("userList", users);
		request.setAttribute("currentPage", "list");
		return "/user/user_list.jsp";
	}
	
	private String createUsersForm(HttpServletRequest request) {
		request.setAttribute("currentPage", "form");
		return "/user/user_form.jsp";
	}

	private String createUser(HttpServletRequest request) {
		UserFacade uf = new UserFacade();
		User user = new User();
		user = fillUser(user, request);
		
		int userId = uf.createUser(user);
		
		return "user?action=view&userId="+userId;
	}
	
	private String editUser(HttpServletRequest request, String userId) {
		UserFacade uf = new UserFacade();
		User user;
		user = uf.loadUser(Integer.parseInt(userId));
		request.setAttribute("user", user);

		request.setAttribute("currentPage", "form");
		return "/user/user_form.jsp";
	}
	
	private String viewUser(HttpServletRequest request, String userId) {
		UserFacade uf = new UserFacade();
		User user;
		user = uf.loadUser(Integer.parseInt(userId));
		request.setAttribute("user", user);
		return "/user/user.jsp";
	}
	
	private String editUser(HttpServletRequest request) {
		UserFacade uf = new UserFacade();
		User user = new User();
		user = fillUser(user, request);
		String id = request.getParameter("userId");
		user.setUserId(Integer.parseInt(id));
		System.out.println("User id: "+user.getUserId());
		
		uf.editUser(user);
		return "user?action=view&userId="+user.getUserId();
	}
	
	private User fillUser(User user , HttpServletRequest request) {
		user.setFirstName(request.getParameter("firstName"));
		user.setSecondName(request.getParameter("secondName"));
		user.setEmail(request.getParameter("email"));
		user.setUsername(request.getParameter("username"));
		user.setPassword(request.getParameter("password"));
		return user;
	}
}
